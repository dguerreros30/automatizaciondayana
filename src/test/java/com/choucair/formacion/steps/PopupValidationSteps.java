package com.choucair.formacion.steps;


import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {
	
	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage ColorlibMenuPage;
	
	@Step
		
	public void login_colorlib(String strUsuario, String strPass) {
		// a. Abrir navegador con la url de prueba
		colorlibLoginPage.open();
		/* b. Ingresar usuario demo
		   c. Ingresar password demo
		   d. Click en botón Sign in  */
		colorlibLoginPage.IngresarDatos(strUsuario, strPass);
		// e. Verificar la Autenticación (Label en home)
		colorlibLoginPage.VerificaHome();
		

	}
	
	@Step
	
	public void ingresar_form_validation() {
		ColorlibMenuPage.menuFormValidation();
	}
	
	public void dilenciar_popup_datos_tabla(List<List<String>> data, int id) {
		
	}
		
}
	

